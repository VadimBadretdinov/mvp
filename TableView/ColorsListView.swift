//
//  ColorsListView.swift
//  TableView
//
//  Created by vadim on 06/07/2019.
//  Copyright © 2019 vadim. All rights reserved.
//

import UIKit

protocol ColorListView: class {
    func setColors(colors: [UIColor])
    func showMessage(colorString: String)
    func refreshColors(colors: [UIColor])
}
