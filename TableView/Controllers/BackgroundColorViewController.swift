//
//  BackgroundColorViewController.swift
//  TableView
//
//  Created by vadim on 07/07/2019.
//  Copyright © 2019 vadim. All rights reserved.
//

import UIKit

class BackgroundColorViewController: UIViewController {
    private var backgroundColor: UIColor = .white
    private let presenter = BackgroundColorPresenter()
    
    @IBOutlet weak var textFieldColor: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.presenter.attachView(view: self)
    }
    
}

extension BackgroundColorViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        if let text = self.textFieldColor.text {
            self.presenter.sendStringToConvert(string: text)
        }
        return true
    }
}

extension BackgroundColorViewController: BackgroundColorView {
    func changeBackgroundColor(color: UIColor) {
        self.view.backgroundColor = color
    }
}
