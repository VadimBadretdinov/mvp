//
//  ViewController.swift
//  TableView
//
//  Created by vadim on 05/07/2019.
//  Copyright © 2019 vadim. All rights reserved.
//

import UIKit

class ColorsListViewController: UIViewController {
    private var colors:[UIColor] = []
    private let presenter = ColorListPresenter()
    private let tableView = UITableView()
    private let refreshControl = UIRefreshControl()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.createTableView()
        self.presenter.attachView(view: self)
        self.presenter.viewLoaded()
    }
    
    private func createTableView() {
        let barHeight: CGFloat = UIApplication.shared.statusBarFrame.size.height
        let displayWidth: CGFloat = UIScreen.main.bounds.width
        let displayHeight: CGFloat = UIScreen.main.bounds.height
        
        self.tableView.frame = CGRect(x: 0, y: barHeight, width: displayWidth, height: displayHeight)
        self.tableView.register(UITableViewCell.self, forCellReuseIdentifier: "color")
        self.tableView.dataSource = self
        self.tableView.delegate = self
        self.setupRefreshControl()
        self.view.addSubview(self.tableView)
    }
    
    private func createAlert(colorString: String) {
        let alert = UIAlertController(title: "Color:", message: colorString, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        self.present(alert,animated: true)
    }
    
    private func setupRefreshControl() {
        self.tableView.refreshControl = self.refreshControl
        self.refreshControl.addTarget(self, action: #selector(self.shuffleColorsList), for: .valueChanged)
    }
    
    @objc private func shuffleColorsList() {
        self.presenter.shuffleColors()
    }
}

extension ColorsListViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.colors.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "color", for: indexPath)
        cell.backgroundColor = self.colors[indexPath.row]
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.tableView.deselectRow(at: indexPath, animated: true)
        self.presenter.colorSelected(color: self.colors[indexPath.row])
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }

    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if (editingStyle == .delete) {
            self.presenter.changeColor(color: self.colors[indexPath.row])
        }
    }
}

extension ColorsListViewController: ColorListView {
    func refreshColors(colors: [UIColor]) {
        self.setColors(colors: colors)
        let deadline = DispatchTime.now() + .milliseconds(700)
        DispatchQueue.main.asyncAfter(deadline: deadline) {
            self.refreshControl.endRefreshing()
        }
    }
    
    
    func showMessage(colorString: String) {
        self.createAlert(colorString: colorString)
    }
    
    func setColors(colors: [UIColor]) {
        self.colors = colors
        self.tableView.reloadData()
    }
    
}
