//
//  ColorListPresenter.swift
//  TableView
//
//  Created by vadim on 06/07/2019.
//  Copyright © 2019 vadim. All rights reserved.
//

import UIKit

class ColorListPresenter {
    
    private weak var colorListView: ColorListView?
    private let model = ColorListModel()
    
    init() {}
    
    func attachView(view: ColorListView) {
        self.colorListView = view
    }
    
    func detachView() {
        self.colorListView = nil
    }
    
    func updateColors() {
        self.colorListView?.refreshColors(colors: self.model.getColorList())
    }
    
    func viewLoaded() {
        self.updateColors()
    }
    
    func colorSelected(color: UIColor) {
        self.colorListView?.showMessage(colorString: self.model.getHexColor(color: color))
    }
    
    func changeColor(color: UIColor) {
        self.model.deleteColorAndSetRandom(colorToDelete: color)
        self.updateColors()
    }
    
    func shuffleColors() {
        self.model.shuffleColors()
        self.updateColors()
    }
}
