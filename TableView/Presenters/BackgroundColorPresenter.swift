//
//  BackgroundColorPresenter.swift
//  TableView
//
//  Created by vadim on 07/07/2019.
//  Copyright © 2019 vadim. All rights reserved.
//

import Foundation

class BackgroundColorPresenter {
    private weak var backgroundColorView: BackgroundColorView?
    private let model = BackgroundColorModel()
    
    init() {}
    
    func attachView(view: BackgroundColorView) {
        self.backgroundColorView = view
    }
    
    func detachView() {
        self.backgroundColorView = nil
    }
    
    func sendStringToConvert(string: String) {
        self.backgroundColorView?.changeBackgroundColor(color: self.model.convertStringToColor(string: string))
    }
}
