//
//  Model.swift
//  TableView
//
//  Created by vadim on 05/07/2019.
//  Copyright © 2019 vadim. All rights reserved.
//

import UIKit
import HexColors

class ColorListModel {
    private var colors:[UIColor] = [.red, .yellow, .green, .blue, .orange]
    
    func getCount() -> Int {
        return self.colors.count
    }
    
    func getColorList() -> [UIColor] {
        return self.colors
    }
    
    func getHexColor(color: UIColor) -> String {
        return color.hex
    }
    
    func deleteColorAndSetRandom(colorToDelete: UIColor) {
        var position = 0
        for color in colors {
            if color == colorToDelete {
                self.colors[position] = self.getRandomColor()
                return
            }
            position += 1
        }
    }
    
    private func getRandomColor() -> UIColor {
        return UIColor(red: CGFloat.random(in: 0...1),
                       green: CGFloat.random(in: 0...1),
                       blue: CGFloat.random(in: 0...1),
                       alpha: 1.0)
    }
    
    func shuffleColors() {
        self.colors.shuffle()
    }
}
