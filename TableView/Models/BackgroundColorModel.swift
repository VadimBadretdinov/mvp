//
//  BackgroundColorModel.swift
//  TableView
//
//  Created by vadim on 07/07/2019.
//  Copyright © 2019 vadim. All rights reserved.
//

import UIKit
import HexColors

class BackgroundColorModel {
    private var color: UIColor = .white
    
    func convertStringToColor(string: String) -> UIColor {
        if let hexColor = UIColor(string) {
            self.color = hexColor
        }
        return color
    }
}
