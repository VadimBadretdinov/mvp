//
//  BackgroundColorView.swift
//  TableView
//
//  Created by vadim on 07/07/2019.
//  Copyright © 2019 vadim. All rights reserved.
//

import UIKit

protocol BackgroundColorView: class {
    func changeBackgroundColor(color: UIColor)
}
